package com.bmobapp.bmobchatai;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import java.util.ArrayList;
import java.util.List;
import cn.bmob.v3.ai.BmobAI;
import cn.bmob.v3.ai.ChatMessageListener;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    EditText messageEditText;
    Button sendButton;

    List<Message> messageList;

    MessageAdapter messageAdapter;

    BmobAI bmobAI;

    static Integer chatnum = 0;
    static String question ="";

    static Integer turn = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("AI 的自言自语");
        //创建Bmob AI实例
        bmobAI = new BmobAI();
        //初始化AI内容问答存储
        messageList = new ArrayList<>();

        recyclerView = findViewById(R.id.msg_recycler_view);
        messageEditText = findViewById(R.id.message_edit_text);
        sendButton = findViewById(R.id.send_bt);

        messageAdapter = new MessageAdapter(messageList);
        recyclerView.setAdapter(messageAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setStackFromEnd(true);
        recyclerView.setLayoutManager(llm);

        //点击发送提问到AI服务器的按钮
        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chatnum = 0;
                //获取问题
                question = messageEditText.getText().toString().trim();
                if (question.isEmpty() || question.trim() == "")
                    return;

                messageEditText.setText("");
                //连接AI服务器（这个代码为了防止AI连接中断）
                bmobAI.Connect();
                turn = 1;
                addToChat(question,Message.SEND_BY_ME);
                autoChat();

            }

        });

    }

    private void autoChat(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (chatnum<20){
                    if(!question.isEmpty()){
                        String tmpQ = question;
                        question = "";
                        if(turn==1){

                            //bmobAI.setPrompt("你现在开始扮演女孩角色，名字叫琪琪，今年28岁，性格活泼开朗，聪明漂亮，做事稳重，说话逻辑严谨。接下来是你和你男朋友小叶子的聊天，内容要有谈情说爱的感觉，每次回复都要能够提出问题，回复保持在20字以内");
                            bmobAI.setPrompt("你现在开始每次都要讲一个内涵段子或者冷笑话，不要小朋友听的那种笑话，不要说有哲理的内容，除了笑话内容之外，不要有其他文字。你也不需要对听众的回复进行解释，继续讲你的笑话就好了。");
                            bmobAI.Chat(tmpQ, "me", new ChatMessageListener() {
                                @Override
                                public void onMessage(String s) {
                                    addToLastMessage(s);
                                }

                                @Override
                                public void onFinish(String s) {
                                    //addToChat(s,Message.SEND_BY_BOT);
                                    question = s;
                                    turn = 2;
                                    chatnum ++;
                                }

                                @Override
                                public void onError(String s) {

                                }

                                @Override
                                public void onClose() {

                                }
                            });
                        }else if(turn==2){
                            //bmobAI.setPrompt("你现在开始扮演男生角色，名字叫小叶子，今年30岁，是一个B站的Up主，喜欢户外旅行。接下来是你和你女朋友琪琪的聊天，说话要简短，内容要有谈情说爱的感觉，回复保持在20字以内。");
                            bmobAI.setPrompt("你现在开始扮演听众，你的目标是怼死对方，不需要感谢对方，不要说对方的笑话好笑，只要怼死对方笑话中的不合理就行。回复内容要小于20个字。");
                            bmobAI.Chat(tmpQ, "girlfriend", new ChatMessageListener() {
                                @Override
                                public void onMessage(String s) {
                                    addToBotLastMessage(s);
                                }

                                @Override
                                public void onFinish(String s) {
                                    //addToChat(s,Message.SEND_BY_ME);
                                    question = s;
                                    turn = 1;
                                    chatnum ++;
                                }

                                @Override
                                public void onError(String s) {

                                }

                                @Override
                                public void onClose() {

                                }
                            });
                        }
                    }


                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }).start();
    }

    /**
     * 支持流的形式呈现内容到界面
     * @param s
     */
    public void addToLastMessage(String s)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(messageList.size()<=0) return;
                Message message =  messageList.get(messageList.size()-1);

                if(message.sendBy==Message.SEND_BY_ME){
                    Message newmessage = new Message(s,Message.SEND_BY_BOT);
                    messageList.add(newmessage);
                    messageAdapter.notifyDataSetChanged();
                    recyclerView.smoothScrollToPosition(messageAdapter.getItemCount());
                }else{
                    message.setMessage(message.getMessage() + s);
                    messageAdapter.notifyDataSetChanged();
                    recyclerView.smoothScrollToPosition(messageAdapter.getItemCount());
                }
            }
        });
    }

    /**
     * 支持流的形式呈现内容到界面
     * @param s
     */
    public void addToBotLastMessage(String s)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(messageList.size()<=0) return;
                Message message =  messageList.get(messageList.size()-1);

                if(message.sendBy==Message.SEND_BY_BOT){
                    Message newmessage = new Message(s,Message.SEND_BY_ME);
                    messageList.add(newmessage);
                    messageAdapter.notifyDataSetChanged();
                    recyclerView.smoothScrollToPosition(messageAdapter.getItemCount());
                }else{
                    message.setMessage(message.getMessage() + s);
                    messageAdapter.notifyDataSetChanged();
                    recyclerView.smoothScrollToPosition(messageAdapter.getItemCount());
                }
            }
        });
    }

    /**
     * 一次性将全部内容呈现到界面
     * @param message
     * @param sendBy
     */
    void addToChat(String message,String sendBy){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageList.add(new Message(message,sendBy));
                messageAdapter.notifyDataSetChanged();
                recyclerView.smoothScrollToPosition(messageAdapter.getItemCount());
            }
        });
    }
}
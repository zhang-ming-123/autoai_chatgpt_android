package com.bmobapp.bmobchatai;

import android.app.Application;

import cn.bmob.v3.Bmob;

public class BmobApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //初始化
        Bmob.initialize(this,"这里的application id要替换为你在https://www.bmobapp.com上面的应用的application id");
    }
}
